#include "kernel/types.h"
#include "kernel/stat.h"
#include "kernel/syscall.h"
#include "user/user.h"

extern char* syscallnames[];

int
main(int argc, char *argv[])
{
  int i;
  int mask, id = 0;
  int pid;

  if (argc < 3) {
    fprintf(2, "Usage: %s <mask> command [args]\n", argv[0]);
  }
  char* argv2[argc-2];

  mask = atoi(argv[1]);
  for (i=0;i<32; i++){
    if ((mask>>i) == 1) {
      id = i;
    } else if ((mask>>i) & 1) {
      fprintf(2, "Error: Mask doesn't correspond to valid syscall\n");
      return 1;
    }
  }
  if (id == 0){
    fprintf(2, "Error: Mask doesn't correspond to valid syscall\n");
    return 1;
  }

  // initialize getsyscount
  if (setSysCount(id) < 0) {
    fprintf(2, "Error: Couldn't initialize system call\n");
    return 1;
  }

  for(i = 2; i < argc; i++){
    argv2[i-2] = argv[i];
  }

  pid = fork();

  if (pid < 0) {
    fprintf(2, "Error: fork failed!\n");
    return 1;
  } else if (pid == 0) {
    exec(argv2[0], argv2);
    fprintf(2, "Error: exec failed!\n");
    return 1;
  } else {
    wait(0);
    char name[30];
    if (getSysName(id, name, 30) < 0) {
       fprintf(2, "Error: Couldn't get name corresponding to syscall\n");
       return 1;
    }
    fprintf(1, "PID %d called %s %d times\n", getpid(), name, getSysCount());
  }
  return 0;
}
