# Intro to Xv6

OSN Monsoon 2024 mini project 2. The whole problem statement can be
found in this
[website](https://karthikv1392.github.io/cs3301_osn/mini-projects/mp2)

## Some pointers

-   main xv6 source code is present inside `initial_xv6/src` directory.
    This is where you will be making all the additions/modifications
    necessary for the xv6 part of the Mini Project.

-   work inside the `networks/` directory for the Networking part of the
    Mini Project.

-   You are free to delete these instructions and add your report before
    submitting.

## Assumptions

## Features

1.  In lottery based scheduling, I am setting a top limit of 100 for
    number of tickets to make the scheduling more realistic and not
    trusting the user.

## Report

### Gotta Count Em All

In the implementation of this, I add three new syscalls `setSysCount`,
`getSysCount` and `getSysName`.

-   The `setSysCount` function sets the variable callid which we are
    supposed to be tracking for throughout the user program. To change
    this, the user must call the setSysCount again with a different mask
    (mask is translated to id in the user program itself).

-   The `getSysCount` function get the existing count of the callid
    variable that has been counted

-   The `getSysName` function is meant to fetch the name of the
    corresponding system call ( I have made my own array in syscall.c to
    define these ).

The main crux of this problem is to add two new parameters to the proc
struct in the `proc.h`, one variable is the syscount and other is the
callid which gets set in the `setSysCount` system call.

-   After every fork, the parent copies its callid variable to next.
-   After every wait, the child that has the ZOMBIE status, before
    getting freed will add its syscount to its parent.

### Wake Me Up When My Timer Ends

In this problem we are required to implement `sigalarm()` and
`sigreturn()`, the actual implementations of these system calls is
simple and has to do with initializing some variables which are added to
the proc struct to store the following info:

-   `alarm_enabled` - the alarm enabled variable to be set so that we
    can later use it to increment or change PC to handler when time runs
    out.
-   `alarm_interval` - the time interval between alarms (first argument
    of the sigalarm)
-   `curr_time` - current time passed after initialization or after
    previous alarm
-   `btf` - Stands for BreakOffTrapframe, which is used to temporarily
    store the TrapFrame of the calling process before and after the call
    is returned using sigreturn.

The `trap.c` is the main part of the code where we track all the
interrupts, using the `which_dev` variable set in the `usertrap()`
function. We can use it to increment `curr_time`, and call the alarm
when its reaches there. Before that we have to store the trapframe in
the btf.

Finally in the sigreturn() syscall we have to restore the trapframe and
free the btf and return the expected return value of the sigalarm's
trapframe which is stored in the `a0` register.

### Scheduling

#### Lottery Based Scheduling (LBS)

This is a better implementation of the original Lottery based ticketing
system which is truly random.

To implement this, we have to first of all add a kernel flag and use a
bunch of `ifdef` tags around the piece of code which is meant to execute
only when LBS is turned on.

By default every processs has 1 ticket. To add support for variable
ticket numbers I have added a system call `settickets()`, which should
set the number of tickets, and is called in the user program. I have
tested this with the schedulertest.c (commented out the calls for
compatibility purposes), which can be used.

Here is the breakdown of what my scheduler function does:

-   It first counts the total number of tickets.
-   Passes it to a random number generator which uses clock time and a
    bunch of arithmetic operations to generate tickets in required
    range.
-   Iterate through all the (runnable) processes to find the required
    ticket.
-   Finally store the ticketsize of above process and again iterate
    through all the processes to find the process with minimum ctime
    (initializing time) and assign it.
-   After the process has been matched, then execute the process by
    changing the myproc and mycpu variable values and locking the
    process.

Finally if you don't find any such process then just break the loop,
because the outer loop will again find you another new random number.

**What is the implication of adding the arrival time in the lottery
based scheduling policy? Are there any pitfalls to watch out for? What
happens if all processes have the same number of tickets?**

Adding the arrival time feature is supposed to make the lottery more
fair but ends up increasing the complexity.

It makes us iterate through the loop multiple times and increases the
time required to find a simple process.

If there is a case where most of the processes have the same lottery
ticket, we will just end up using FCFS, and end up having the same
issues as FCFS like processes which take long to execute but arrive
first blocking the resources.

Finally it just takes away the essense of lottery based scheduling which
is a form of linear monte-carlo prediction, which is able to pick the
process with more number of tickets more often. But even the lottery
scheduling has a fundamental flaw that we are trusting the user when
writing kernel code for them.

#### MLFQ (Multi-Level Queues)

In my MLFQ code I am introducing only a single new variable to the proc
struct which is `int queue` which stores the queue number of each
process. I also globally store the timeslice for each of the 4 queues.

To handle the boost i am just taking modulo of the ticks variable with
48ms. If the value is 0 then its the time to boost and make all the
queue values of all the processes to 0.

Now everytime the clock ticks, the scheduler will iterate through all
the NPROCS first find the head (topmost queue with runable processes)
and then construct that head queue by finding all the processes in that
queue, since there are only 64 procs, this was relatively faster for me,
than storing the queues in memory.

For every queue, I just iterate through all the processes and execute
them, just like round-robin, but increment a variable called `curr`
which keeps track of time.

There are three possibilities while this round-robin is happening:

-   The curr reaches the timeslice of the queue in which case the
    current process in question is demoted and the queue is constructed
    again.
-   There is a new process so the headqueue will change, and require
    just to stop and reconstruct the head queue.
-   The tick hits multiple of 48, in which case boost takes place.

Overall to store the outputs of these I just used a print statement
whenever the tick variable increments. Since I am already iterating
through the procs, I just print the pid and the queue number of each
process in each tick.

Here's the plot that I constructed from the values from that print
function.

![MLFQ Plot](./initial-xv6/src/plot.png)

The plot clearly has the boosts around 96ms, 144ms, 192ms etc. showing
how the boost gets triggered then ticks % 48 is 0. Also not how most
processes get stuck in the bottom most queue after sometime. In such
cases reducing the boost time or increasing the number of queues helps.

## Networking

### XOXO

The XOXO game for both TCP and UDP remains same in the basic logic. You
have to send the grid to the client everytime, and when the grid is
reached the client knows that it is its turn. If the grid contains 'L'
or 'D', it means stop playing and you lost or you won respectively, then
we stop the game and declare reult.

The client takes user input for the index, it validates it. If wrong
then reprompts. Then if it is valid it sends the index to the server.
The server then sends a Winner bit (boolean), telling whether to
continue the game or stop and declare win.

The way we go back to the original state differs in both: Here is the
general procedure

-   For Server Initialization Bind the servdr socket to a specific port.
    Initialize game state (empty board, player turns, etc.).

-   Confirmation messages logic change between UDP and TCP. Server
    assigns 'X' and 'O' to clients and sends back confirmations.

-   Server waits for a move from the current player using recvfrom().
    Upon receiving a move, server updates game state and checks for
    win/draw. Server sends updated game state to both clients using
    sendto(). If game is not over, server waits for next player's move.

-   Server validates each move (correct player, legal move, etc.). If
    move is invalid, server sends error message and waits for new move.
    Implement timeouts to handle cases where a client becomes
    unresponsive.

-   With each valid move, send full game state (grid) to both clients.
    Clients update their local game state upon receiving server update.
    This ensures all parties have consistent view of the game.

-   When win or draw is detected, server sends game result to both
    clients. Clients display result and close their sockets. Server
    resets game state for a new game. Here it again waits for 2
    connections to be available

#### TCP

For TCP you have to follow the following procedure:

-   The client waits for connections to be available using the accept()
    commands. Until the two connections get available the server waits.
    Once the clients connect we assign them symbols. In TCP this
    assignment only occurs once because over different runs of the game
    we preserve the connections.
-   In TCP if the connection gets lost once, both the server and the
    client reset.

#### UDP

Here's the procedure I followed for UDP:

-   The server waits for two connections to become available before, it
    startss the game, and it doesn't depend on who it is.

-   There is no connection steps. Based on the order of confirmation
    messages, it assigns 'X' or 'O' everytime differently

-   After every game end, it again waits for confirmation messages

-   One advantage in my implementation is that, if one of the client
    exits, it doesn't fail the server nor reset the game, it infact
    reassigns the symbol for the lost connections and continues the
    game.

### Fake it till you make it

The main idea is to implement the "turns" functionality, where the
client is knowns if it is its turn to send (tracked using) `is_sending`
variable or if the server is sending `is_recieving` variable in the
server.

-   I have used non-blocking sockets, so none of the recv calls wait for
    the client to send the bit and move on instantly. I have used
    `fcntl` to set the non blocking flag.

-   Each chunk contains (Packet struct), the sequence number, total
    number of chunks, and finally the data. I have assumed that each
    chunk contains a string of length 4 which is stored in the data
    variable.

-   When recieving, we initialize an array of chunks (boolean array),
    and in a while loop, we recieve all the chunks one by one and the
    array is in such a way that the index of the array matches the
    sequence number of the chunk. So when we recieve a chunk, we mark
    True in this array and also send a corresponding ACK variable which
    is the same as the sequence number. Once this boolean array has True
    for all the chunks then we send final ACK bit.

-   When sending, we first make a chunks array out of the input and
    store index i. We send the chunk and initialize a timeout variable
    for that send() we did. Now we won't wait for the ACK bit but be
    open to recieve ACK bits using recv (this happens because of
    non-blocking). We use the select() function to track the activity of
    the connection and it exceeds the timeout, we resend the packet and
    decrement the chunk index so that packet gets sent again.

-   There is a wierd edge case where if you don't recieve ACK for the
    last chunk, the reciever's boolean array is full so it moves on to
    send mode (prolly takes user input for the next send). But the
    sender still thinks the ACK is not recieved so it has to resend.
    This is handled by using a `MAX_RETRANSMISSION` variable which sends
    the packet only that number of times, then just gives up.
