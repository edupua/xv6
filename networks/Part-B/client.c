#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#define PORT "12452"
#define IP "127.0.0.1"
#define BUFFER_SIZE 1024
#define MAX_CHUNKS 1000
#define CHUNK_SIZE 4
#define TIMEOUT_US 100000 // 0.1s
#define MAX_RETRANSMISSIONS 10

struct Packet {
  int seq_num;
  int total_chunks;
  char data[CHUNK_SIZE];
};

int
main() {
  int client_socket;
  struct addrinfo hints, *results, *p;
  char buffer[BUFFER_SIZE];
  struct Packet chunks[MAX_CHUNKS];
  int received_chunks[MAX_CHUNKS] = {0};
  int total_chunks = 0;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;

  if ((getaddrinfo(IP, PORT, &hints, &results)) != 0) {
    perror("Client socket translation failed");
    return 1;
  }

  for (p = results; p != NULL; p = p->ai_next) {
    if ((client_socket =
             socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }
    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to create socket\n");
    return 2;
  }

  int flags = fcntl(client_socket, F_GETFL, 0);
  fcntl(client_socket, F_SETFL, flags | O_NONBLOCK);

  int is_sending = 1;

  while (1) {
    if (is_sending) {
      printf("The client is asking for input: ");
      fgets(buffer, BUFFER_SIZE, stdin);
      buffer[strcspn(buffer, "\n")] = 0;

      int message_len = strlen(buffer);
      int num_chunks = (message_len + CHUNK_SIZE - 1) / CHUNK_SIZE;
      int bad_count = 0;

      for (int i = 0; i < num_chunks; i++) {
        struct Packet packet;
        packet.seq_num = i;
        packet.total_chunks = num_chunks;
        strncpy(packet.data, buffer + (i * CHUNK_SIZE), CHUNK_SIZE);

        sendto(client_socket, &packet, sizeof(packet), 0, p->ai_addr,
               p->ai_addrlen);

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = TIMEOUT_US;

        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(client_socket, &readfds);

        int activity = select(client_socket + 1, &readfds, NULL, NULL, &tv);

        if (activity > 0) {
          int ack;
          struct sockaddr_storage server_addr;
          socklen_t addr_len = sizeof server_addr;
          recvfrom(client_socket, &ack, sizeof(ack), 0,
                   (struct sockaddr *)&server_addr, &addr_len);
          printf("Received ACK for chunk %d\n", ack);
          bad_count = 0;
        } else {
          if (bad_count == MAX_RETRANSMISSIONS) {
            printf("Max Retransition limit reached. Seems like the server is "
                   "in input mode.\n");
            break;
          }
          printf("Timeout occurred for chunk %d, retransmitting\n", i + 1);
          bad_count++;
          i--;
        }
      }

      is_sending = 0;
    } else {
      struct Packet packet;
      struct sockaddr_storage server_addr;
      socklen_t addr_len = sizeof server_addr;
      int n = recvfrom(client_socket, &packet, sizeof(packet), 0,
                       (struct sockaddr *)&server_addr, &addr_len);

      if (n < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
          continue;
        } else {
          perror("recvfrom failed");
          exit(EXIT_FAILURE);
        }
      }

      if (packet.seq_num < MAX_CHUNKS) {
        chunks[packet.seq_num] = packet;
        received_chunks[packet.seq_num] = 1;
        total_chunks = packet.total_chunks;

        int ack = packet.seq_num;
        // int r = rand() % 3;
        // if (r != 0)
        sendto(client_socket, &ack, sizeof(ack), 0,
               (struct sockaddr *)&server_addr, addr_len);

        printf("Received chunk %d of %d\n", packet.seq_num + 1, total_chunks);

        int all_received = 1;
        for (int i = 0; i < total_chunks; i++) {
          if (!received_chunks[i]) {
            all_received = 0;
            break;
          }
        }

        if (all_received) {
          printf("All chunks received. Reconstructed message:\n");
          for (int i = 0; i < total_chunks; i++) {
            printf("%s", chunks[i].data);
          }
          printf("\n");

          memset(received_chunks, 0, sizeof(received_chunks));
          total_chunks = 0;
          is_sending = 1;
        }
      }
    }
  }

  freeaddrinfo(results);
  close(client_socket);
  return 0;
}
