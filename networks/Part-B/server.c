#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <unistd.h>

#define PORT "12452"
#define BUFFER_SIZE 1024
#define MAX_CHUNKS 1000
#define CHUNK_SIZE 4
#define TIMEOUT_US 100000 // 0.1 sec
#define MAX_RETRANSMISSIONS 10

struct Packet {
  int seq_num;
  int total_chunks;
  char data[CHUNK_SIZE];
};

int
main() {
  int server_socket;
  struct addrinfo hints, *results, *p;
  char buffer[BUFFER_SIZE];
  struct Packet chunks[MAX_CHUNKS];
  int received_chunks[MAX_CHUNKS] = {0};
  int total_chunks = 0;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  if ((getaddrinfo(NULL, PORT, &hints, &results)) != 0) {
    perror("Server socket translation failed");
    return 1;
  }

  for (p = results; p != NULL; p = p->ai_next) {
    if ((server_socket =
             socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      perror("server: socket");
      continue;
    }

    if (bind(server_socket, p->ai_addr, p->ai_addrlen) == -1) {
      close(server_socket);
      perror("server: bind");
      continue;
    }

    break;
  }

  if (p == NULL) {
    fprintf(stderr, "server: failed to bind socket\n");
    return 2;
  }

  freeaddrinfo(results);

  int flags = fcntl(server_socket, F_GETFL, 0);
  fcntl(server_socket, F_SETFL, flags | O_NONBLOCK);

  printf("Server listening on port %s...\n", PORT);

  int is_receiving = 1;
  struct sockaddr_storage client_addr;
  socklen_t addr_len = sizeof client_addr;

  while (1) {
    if (is_receiving) {
      struct Packet packet;
      int n = recvfrom(server_socket, &packet, sizeof(packet), 0,
                       (struct sockaddr *)&client_addr, &addr_len);

      if (n < 0) {
        if (errno == EWOULDBLOCK || errno == EAGAIN) {
          continue;
        } else {
          perror("recvfrom failed");
          exit(EXIT_FAILURE);
        }
      }

      if (packet.seq_num < MAX_CHUNKS) {
        chunks[packet.seq_num] = packet;
        received_chunks[packet.seq_num] = 1;
        total_chunks = packet.total_chunks;

        int ack = packet.seq_num;
        int r = rand() % 3;
        if (r != 0)
          sendto(server_socket, &ack, sizeof(ack), 0,
               	(struct sockaddr *)&client_addr, addr_len);

        printf("chunks received %d of %d\n", packet.seq_num + 1, total_chunks);

        int all_received = 1;
        for (int i = 0; i < total_chunks; i++) {
          if (!received_chunks[i]) {
            all_received = 0;
            break;
          }
        }

        if (all_received) {
          printf("\nReconstructed message: ");
          for (int i = 0; i < total_chunks; i++) {
            printf("%s", chunks[i].data);
          }
          printf("\n\n");

          memset(received_chunks, 0, sizeof(received_chunks));
          total_chunks = 0;
          is_receiving = 0;
        }
      }
    } else {
      printf("The server is asking for input: ");
      fgets(buffer, BUFFER_SIZE, stdin);
      buffer[strcspn(buffer, "\n")] = 0;

      int message_len = strlen(buffer);
      int num_chunks = (message_len + CHUNK_SIZE - 1) / CHUNK_SIZE;
      int bad_count = 0;

      for (int i = 0; i < num_chunks; i++) {
        struct Packet send_packet;
        send_packet.seq_num = i;
        send_packet.total_chunks = num_chunks;
        strncpy(send_packet.data, buffer + (i * CHUNK_SIZE), CHUNK_SIZE);

        sendto(server_socket, &send_packet, sizeof(send_packet), 0,
               (struct sockaddr *)&client_addr, addr_len);

        struct timeval tv;
        tv.tv_sec = 0;
        tv.tv_usec = TIMEOUT_US;

        fd_set readfds;
        FD_ZERO(&readfds);
        FD_SET(server_socket, &readfds);

        int activity = select(server_socket + 1, &readfds, NULL, NULL, &tv);

        if (activity > 0) {
          int recv_ack;
          recvfrom(server_socket, &recv_ack, sizeof(recv_ack), 0,
                   (struct sockaddr *)&client_addr, &addr_len);
          printf("ACK: %d recieved\n", recv_ack);
          bad_count = 0;
        } else {
          if (bad_count == MAX_RETRANSMISSIONS) {
            printf("Max Retransition limit reached. Seems like the server is "
                   "in input mode.\n");
            break;
          }
          printf("chunk %d timedout, retransmitting\n", i + 1);
          bad_count++;
          i--;
        }
      }

      is_receiving = 1;
    }
  }

  close(server_socket);
  return 0;
}
