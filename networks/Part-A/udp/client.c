
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define IP "127.0.0.1"
#define PORT "23236"

int client_socket = -1;

void
cleanup() {
  if (client_socket >= 0)
    close(client_socket);
  exit(EXIT_SUCCESS);
}

void
print_grid(char grid[3][3]) {
  printf("The grid is: \n");
  printf("\t-------------\n");
  for (int i = 0; i < 3; i++) {
    printf("\t| %c | %c | %c |\n", grid[i][0], grid[i][1], grid[i][2]);
    printf("\t-------------\n");
  }
}

void
check_error(int retval) {
  if (retval <= 0) {
    if (retval < 0) {
      perror("Error in socket operation");
    } else {
      fprintf(stderr, "Connection closed by server unexpectedly\n");
    }
    if (client_socket > 0)
      close(client_socket);
    exit(EXIT_FAILURE);
  }
}

int
main() {
  struct addrinfo hints, *results, *p;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;

  if ((getaddrinfo(IP, PORT, &hints, &results)) != 0) {
    perror("Client socket translation failed");
    return 1;
  }

  for (p = results; p != NULL; p = p->ai_next) {
    if ((client_socket =
             socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      perror("client: socket");
      continue;
    }
    break;
  }

  if (p == NULL) {
    fprintf(stderr, "client: failed to create socket\n");
    cleanup();
  }

  while (1) {
    fprintf(stdout, "Do you want to play? (y/n)\n");
    char start = getchar();
    if (start != 'y') {
      fprintf(stderr, "Quitting...");
      break;
    }
    int n = getchar();

    // tell server you are ready
    int ready = -2;
    check_error(sendto(client_socket, &ready, sizeof ready, 0, p->ai_addr,
                       p->ai_addrlen));

    // recieve the char
    int pos = -1, i, j;
    char chose = 0;
    struct sockaddr_storage server_addr;
    socklen_t addr_len;
    check_error(recvfrom(client_socket, &chose, sizeof chose, 0,
                         (struct sockaddr *)&server_addr, &addr_len));
    if (chose == 0) {
      fprintf(stderr, "Couldn't recieve the token from server\n");
      cleanup();
    }
    fprintf(stderr, "You have been assigned %c\n", chose);

    while (1) {
      fputs("\033c", stdout);
      char grid[3][3];
      check_error(recvfrom(client_socket, &grid, sizeof grid, 0,
                           (struct sockaddr *)&server_addr, &addr_len));
      if (grid[0][0] == 'L') {
        printf("You lost womp womp...\n");
        break;
      }
      if (grid[0][0] == 'D') {
        printf("Match Tied...\n");
        break;
      }
      print_grid(grid);
      while (1) {
        printf("\nChoose Wisely. Your symbol is '%c'.\nEnter the position in "
               "form (i j) (without brackets) from 1 to 3: \n",
               chose);
        i = getchar();
        int t = getchar();
        j = getchar();
        int n = getchar();
        if (i > '3' || i < '1' || j > '3' || j < '1' || t != ' ' || n != '\n') {
          printf("Invalid input: Check Syntax\n");
        } else if (grid[i - '1'][j - '1'] != ' ') {
          printf("Invalid input: Cell Occupied\n");
        } else {
          pos = (i - '1') * 3 + (j - '1');
          break;
        }
      }
      check_error(sendto(client_socket, &pos, sizeof pos, 0, p->ai_addr,
                         p->ai_addrlen));
      int winner = 0;
      check_error(recvfrom(client_socket, &winner, sizeof winner, 0,
                           (struct sockaddr *)&server_addr, &addr_len));
      if (winner > 0) {
        fprintf(stderr, "You won the match !!!\n");
        break;
      } else {
        fputs("\033c", stdout);
        printf("\nWait for your opponent's turn\n");
      }
    }
    fprintf(stderr, "Trying to start a new game\n");
  }
  close(client_socket);
}
