#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT "23236"

int server_socket = -1;

void
check_error(int retval) {
  if (retval <= 0) {
    if (retval < 0) {
      perror("Error in socket operation");
    } else {
      fprintf(stderr,
              "Packet from on of the clients was unable to reach, exiting\n");
    }
    if (server_socket > 0)
      close(server_socket);
    exit(EXIT_FAILURE);
  }
}

int
test_grid(char grid[3][3]) {

  printf("The grid is: \n");
  printf("\t-------------\n");
  for (int i = 0; i < 3; i++) {
    printf("\t| %c | %c | %c |\n", grid[i][0], grid[i][1], grid[i][2]);
    printf("\t-------------\n");
  }

  // check rows and cols
  for (int i = 0; i < 3; i++) {
    if ((grid[i][0] != ' ' && grid[i][0] == grid[i][1] &&
         grid[i][1] == grid[i][2]) ||
        (grid[0][i] != ' ' && grid[0][i] == grid[1][i] &&
         grid[1][i] == grid[2][i])) {
      return 1;
    }
  }
  // check both diagonals
  if ((grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]) ||
      (grid[2][0] == grid[1][1] && grid[1][1] == grid[0][2])) {
    if (grid[1][1] != ' ')
      return 1;
  }
  return 0;
}

int
is_full(char grid[3][3]) {
  int full = 1;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (grid[i][j] == ' ') {
        full = 0;
        break;
      }
    }
  }
  return full;
}

int
main() {
  struct addrinfo hints, *results, *p;
  struct sockaddr_storage client_addr1, client_addr2;
  socklen_t len_client1, len_client2;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = AI_PASSIVE;

  if ((getaddrinfo(NULL, PORT, &hints, &results)) != 0) {
    perror("Server socket translation failed");
    return 1;
  }

  for (p = results; p != NULL; p = p->ai_next) {
    if ((server_socket =
             socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      perror("server: socket");
      continue;
    }

    if (bind(server_socket, p->ai_addr, p->ai_addrlen) == -1) {
      close(server_socket);
      perror("server: bind");
      continue;
    }

    break;
  }

  if (p == NULL) {
    fprintf(stderr, "server: failed to bind socket\n");
    return 2;
  }

  freeaddrinfo(results);

  printf("Server listening on port %s\n", PORT);

  // send their chars to them
  printf("Sending symbols to clients\n");
  while (1) {
    memset((struct sockaddr *)&client_addr1, 0, sizeof(client_addr1));
    memset((struct sockaddr *)&client_addr2, 0, sizeof(client_addr2));
    len_client1 = sizeof(client_addr1);
    len_client2 = sizeof(client_addr2);
    int conf1 = 0, conf2 = 0;

    // get confirmation messages from both, 2 knows to wait
    fprintf(stderr, "Server waiting for 2 players.\n");
    check_error(recvfrom(server_socket, &conf1, sizeof conf1, 0,
                         (struct sockaddr *)&client_addr1, &len_client1));
    if (conf1 != -2) {
      fprintf(stderr, "Confirmation from 1 not reached properly.\n");
    }
    printf("Confirmation from 1 reached, Waiting for 1 more player\n");
    char symbols[2] = {'X', 'O'};
    check_error(sendto(server_socket, &symbols[0], sizeof symbols, 0,
                       (struct sockaddr *)&client_addr1, len_client1));

    // get confirmation from 2
    check_error(recvfrom(server_socket, &conf2, sizeof conf2, 0,
                         (struct sockaddr *)&client_addr2, &len_client2));
    if (conf2 != -2) {
      fprintf(stderr, "Confirmation from 2 not reached properly.\n");
      continue;
    }
    printf("Confirmation from 2 reached\n");
    check_error(sendto(server_socket, &symbols[1], sizeof symbols, 0,
                       (struct sockaddr *)&client_addr2, len_client2));
    printf("Sent respective symbols to them\n\n");

    // start the game
    int turn = 0, winner = 0; // 0 means 1's turn
    struct sockaddr_storage curr_addr, waiter_addr;
    socklen_t curr_len, waiter_len;
    char grid[3][3];
    // initialize the grid
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        grid[i][j] = ' ';

    while (!is_full(grid)) {

      // write manual instead of exchange logic;
      if (turn == 0) {
        curr_addr = client_addr1;
        waiter_addr = client_addr2;
        curr_len = len_client1;
        waiter_len = len_client2;
      } else {
        curr_addr = client_addr2;
        waiter_addr = client_addr1;
        curr_len = len_client2;
        waiter_len = len_client1;
      }

    reconnect:
      printf("Player %d's turn\n", turn + 1);
      check_error(sendto(server_socket, &grid, sizeof grid, 0,
                         (struct sockaddr *)&curr_addr, curr_len));
      int index = -1;
      check_error(recvfrom(server_socket, &index, sizeof index, 0,
                           (struct sockaddr *)&curr_addr, &curr_len));
      if (index < 0) {
        if (index == -2) {
          printf("Player %d Reconnected !\n", turn + 1);
          check_error(sendto(server_socket, &symbols[turn], sizeof symbols, 0,
                             (struct sockaddr *)&client_addr2, len_client2));
          goto reconnect;
        } else {
          fprintf(stderr, "Client %d Couldn't recieve token from parent\n",
                  turn + 1);
          break;
        }
      }
      grid[index / 3][index % 3] = symbols[turn];

      if (test_grid(grid)) {
        winner = turn + 1;
        // send winning message
        check_error(sendto(server_socket, &winner, sizeof(winner), 0,
                           (struct sockaddr *)&curr_addr, curr_len));
        grid[0][0] = 'L'; // send losing message (the client expects grid)
        check_error(sendto(server_socket, &grid, sizeof(grid), 0,
                           (struct sockaddr *)&waiter_addr, waiter_len));
        break;
      } else {
        // send keepgoing message
        winner = 0;
        check_error(sendto(server_socket, &winner, sizeof(winner), 0,
                           (struct sockaddr *)&curr_addr, curr_len));
      }
      turn = !turn;
    }
    if (winner > 0)
      printf("Winner is Player %d!!\n\n", winner);
    else {
      printf("Its a Draw !\n\n");
      grid[0][0] = 'D';
      check_error(sendto(server_socket, &grid, sizeof(grid), 0,
                         (struct sockaddr *)&curr_addr, curr_len));
      check_error(sendto(server_socket, &grid, sizeof(grid), 0,
                         (struct sockaddr *)&waiter_addr, waiter_len));
    }
  }
  fprintf(stderr, "Closing Connection! Bye.\n");
  close(server_socket);
}
