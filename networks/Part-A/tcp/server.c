#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define PORT "23235"

int server_socket = -1, client_socket1 = -1, client_socket2 = -1;

void
cleanup() {
  if (server_socket != -1)
    close(server_socket);
  if (client_socket1 != -1)
    close(client_socket1);
  if (client_socket2 != -1)
    close(client_socket2);
  exit(EXIT_SUCCESS);
}

int
test_grid(char grid[3][3]) {

  printf("The grid is: \n");
  printf("\t-------------\n");
  for (int i = 0; i < 3; i++) {
    printf("\t| %c | %c | %c |\n", grid[i][0], grid[i][1], grid[i][2]);
    printf("\t-------------\n");
  }

  // check rows and cols
  for (int i = 0; i < 3; i++) {
    if ((grid[i][0] != ' ' && grid[i][0] == grid[i][1] &&
         grid[i][1] == grid[i][2]) ||
        (grid[0][i] != ' ' && grid[0][i] == grid[1][i] &&
         grid[1][i] == grid[2][i])) {
      return 1;
    }
  }
  // check both diagonals
  if ((grid[0][0] == grid[1][1] && grid[1][1] == grid[2][2]) ||
      (grid[2][0] == grid[1][1] && grid[1][1] == grid[0][2])) {
    if (grid[1][1] != ' ')
      return 1;
  }
  return 0;
}

int
is_full(char grid[3][3]) {
  int full = 1;
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (grid[i][j] == ' ') {
        full = 0;
        break;
      }
    }
  }
  return full;
}

void
check_error(int retval) {
  if (retval <= 0) {
    if (retval < 0) {
      perror("Error in socket operation");
    } else {
      fprintf(stderr, "Connection closed by one of the clients\n");
    }
    close(server_socket);
    close(client_socket1);
    close(client_socket2);
    exit(EXIT_FAILURE);
  }
}

int
main() {

  // close sockets on exit
  signal(SIGINT, cleanup);
  signal(SIGTERM, cleanup);

  // initialize connection with first client
  //
  struct addrinfo hints, *results, *p;
  int num_connections = 0;
  char grid[3][3];
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  if ((getaddrinfo(NULL, PORT, &hints, &results)) != 0) {
    perror("Server Socket Translation failed");
    exit(EXIT_FAILURE);
  }

  for (p = results; p; p = p->ai_next) {
    if ((server_socket =
             socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      perror("client: socket");
      exit(EXIT_FAILURE);
    }

    int opt = 1;
    if (setsockopt(server_socket, SOL_SOCKET, SO_REUSEPORT, &opt,
                   sizeof(opt)) == -1) {
      perror("setsockopt SO_REUSEPORT");
      exit(1);
    }

    if (bind(server_socket, p->ai_addr, p->ai_addrlen) == -1) {
      close(server_socket);
      perror("server: bind");
      exit(EXIT_FAILURE);
    }

    break;
  }

  freeaddrinfo(results);

  if (listen(server_socket, 2) == -1) {
    perror("Listen on socket failed");
    close(server_socket);
    exit(EXIT_FAILURE);
  }

  fprintf(stderr, "Server started at port %s\n\n", PORT);

  fprintf(stderr, "Server waiting for 2 more Clients to start game\n");

  // wait to initalize connection
  while (1) {
    int temp_socket = 0;
    struct sockaddr temp_addr;
    socklen_t temp_length = sizeof(temp_addr);

    if ((temp_socket = accept(server_socket, &temp_addr, &temp_length)) < 0) {
      perror("Failed to accept client");
      cleanup();
      exit(EXIT_FAILURE);
    }

    if (temp_socket >= 0) {
      if (client_socket1 < 0) {
        client_socket1 = temp_socket;
        int value = 1;
        fprintf(stderr, "Server waiting for 1 more Client\n");
      } else if (client_socket2 < 0) {
        client_socket2 = temp_socket;
        break;
      } else {
        fprintf(stderr,
                "Something went wrong when connecting shouldn't be here\n");
      }
    }
  }

  // each iteration of game
  char chose = 'X';
  send(client_socket1, &chose, sizeof(chose), 0); // sending that 'X' is 1's no
  chose = 'O';
  send(client_socket2, &chose, sizeof(chose), 0); // sending that 'O' is 2's no
  while (1) {
    // waiting for confirmation from both players
    fprintf(stderr, "\nServer waiting for Confirmation from both players\n\n");
    int value = 0;
    int play = 0;
    check_error(recv(client_socket1, &value, sizeof(value), 0));
    fprintf(stderr, "One seems ready\n");
    check_error(
        send(client_socket2, &value, sizeof(chose), 0)); // tell 2 im ready
    value = 0;
    check_error(recv(client_socket2, &value, sizeof(value), 0));
    fprintf(stderr, "Player1 is 'X' and Player2 is 'O'\nStarting the game!\n");

    // initialize grid
    for (int i = 0; i < 3; i++)
      for (int j = 0; j < 3; j++)
        grid[i][j] = ' ';

    int turn = 0;
    int curr = client_socket1;
    int waiter = client_socket2;
    int winner = 0;
    while (!is_full(grid)) {
      printf("Player %d's move...\n", turn + 1);
      check_error(send(curr, &grid, sizeof(grid), 0)); // send player grid
      int index = -1;
      check_error(recv(curr, &index, sizeof(index), 0)); // recieve index

      // server assumes index is valid
      if (turn)
        grid[index / 3][index % 3] = 'O';
      else
        grid[index / 3][index % 3] = 'X';
      if (test_grid(grid)) {
        winner = turn + 1;
        // send winning message
        check_error(send(curr, &winner, sizeof(winner), 0));
        grid[0][0] = 'L'; // send losing message (the client expects grid)
        check_error(send(waiter, &grid, sizeof(grid), 0));
        break;
      } else {
        // send keepgoing message
        check_error(send(curr, &winner, sizeof(winner), 0));
      }

      // switch the turn
      int temp = curr;
      curr = waiter;
      waiter = temp;
      turn = !turn;
    }
    if (winner)
      printf("Winner is Player %d!!\n\n", winner);
    else {
      printf("Its a Draw !\n\n");
      grid[0][0] = 'D';
      check_error(send(curr, &grid, sizeof(grid), 0));
      check_error(send(waiter, &grid, sizeof(grid), 0));
    }
  }

  fprintf(stderr, "Closing Connection! Bye.\n");
  close(client_socket1);
  close(client_socket2);
  close(server_socket);
}
