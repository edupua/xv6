#include <netdb.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#define IP "127.0.0.1"
#define PORT "23235"

int client_socket = -1;

void
cleanup() {
  if (client_socket >= 0)
    close(client_socket);
  exit(EXIT_SUCCESS);
}

void
print_grid(char grid[3][3]) {
  printf("The grid is: \n");
  printf("\t-------------\n");
  for (int i = 0; i < 3; i++) {
    printf("\t| %c | %c | %c |\n", grid[i][0], grid[i][1], grid[i][2]);
    printf("\t-------------\n");
  }
}

void
check_error(int retval) {
  if (retval <= 0) {
    if (retval < 0) {
      perror("Error in socket operation");
    } else {
      fprintf(stderr, "Connection closed by server unexpectedly\n");
    }
    if (client_socket > 0)
      close(client_socket);
    exit(EXIT_FAILURE);
  }
}

int
main() {

  // when unexpectedly closed
  signal(SIGINT, cleanup);
  signal(SIGTERM, cleanup);

  // initialize connection with first client
  struct addrinfo hints, *results, *p;
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_protocol = IPPROTO_TCP;

  if ((getaddrinfo(IP, PORT, &hints, &results)) != 0) {
    perror("Client Socket Translation failed");
    exit(EXIT_FAILURE);
  }

  for (p = results; p; p = p->ai_next) {
    if ((client_socket =
             socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      perror("client: socket");
      close(client_socket);
      exit(EXIT_FAILURE);
    }
    if (connect(client_socket, p->ai_addr, p->ai_addrlen) == -1) {
      perror("client: connect");
      close(client_socket);
      continue;
    }
    break;
  }
  if (!p) {
    perror("Couldn't find connection check IP and PORT");
    exit(EXIT_FAILURE);
  }

  freeaddrinfo(results);

  // receive the number
  char chose = 0;

  int pos = -1, i, j;
  check_error(recv(client_socket, &chose, sizeof(chose), 0));
  if (!chose) {
    fprintf(stderr, "Server terminated unexpectedly");
    close(client_socket);
    return 1;
  }
  fprintf(stderr, "You have been assigned %c\n", chose);
  while (1) {
    fprintf(stdout, "Do you want to start new game? (y/n)\n");
    char start = getchar();
    if (start != 'y') {
      fprintf(stderr, "Quitting...");
      break;
    }
    int n = getchar();
    if (chose != 'X') {
      int temp;
      // If you are Player 2 then wait for other player
      fprintf(stderr, "Waiting for other Player's reply\n");
      check_error(recv(client_socket, &temp, sizeof temp, 0));
    }
    // tell server you are ready
    int ready = 1;
    check_error(send(client_socket, &ready, sizeof ready, 0));
    while (1) {
      fputs("\033c", stdout);
      char grid[3][3];
      check_error(recv(client_socket, &grid, sizeof grid, 0));
      if (grid[0][0] == 'L') {
        printf("You lost womp womp...\n");
        break;
      }
      if (grid[0][0] == 'D') {
        printf("Match Tied...\n");
        break;
      }
      print_grid(grid);
      while (1) {
        printf("\nChoose Wisely. Your symbol is '%c'.\nEnter the position in "
               "form (i j) (without brackets) from 1 to 3: \n",
               chose);
        i = getchar();
        int t = getchar();
        j = getchar();
        int n = getchar();
        if (i > '3' || i < '1' || j > '3' || j < '1' || t != ' ' || n != '\n') {
          printf("Invalid input: Check Syntax\n");
        } else if (grid[i - '1'][j - '1'] != ' ') {
          printf("Invalid input: Cell Occupied\n");
        } else {
          pos = (i - '1') * 3 + (j - '1');
          break;
        }
      }
      check_error(send(client_socket, &pos, sizeof pos, 0));
      int winner = 0;
      check_error(recv(client_socket, &winner, sizeof winner, 0));
      if (winner > 0) {
        fprintf(stderr, "You won the match !!!\n");
        break;
      } else {
        fputs("\033c", stdout);
        printf("\nWait for your opponent's turn\n");
      }
    }
    fprintf(stderr, "Trying to start a new game\n");
  }
  close(client_socket);
}
